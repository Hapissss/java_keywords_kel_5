/*
throws

Keyword throws digunakan dalam suatu method atau kelas yang mungkin menghasilkan suatu kesalahan 
sehingga perlu ditangkap errornya. Cara mendefinisikannya dalam method adalah sebagai berikut :
<method modifier> type method-name throws exception-list1, exceptio-list2, … {}.
membahas mengenai teknik yang digunakan pada java untuk menangani masalah yang memungkinkan
program berjalan tidak normal, teknik ini dinamakan exception handling,
ada 5 jenis kata kunci/keyword penting pada java dalam hal exception handling,
yaitu try, catch, finally, throw dan throws.

*/

public class belajarThrows
{
    public static void main(String[] args) {
        try
        {
            f();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }

    public static void f() throws NullPointerException, ArrayIndexOutOfBoundsException
    {
        //implementasi method
        throw new NullPointerException();
        //throw new ArrayIndexOutOfBoundsException();
    }
}