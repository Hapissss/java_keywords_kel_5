/*
Java memiliki sebuah fitur Serialization yang berguna untuk membekukan sebuah objek dan menyimpannya 
ke dalam permanent storage maupun dikirim ke sebuah tempat tertentu. Objek ini dapat dikembalikan
ke keadaan semula dan dapat digunakan seperti biasa.
Caranya? Cukup buat sebuah kelas yang mengimplements java.io.
Serializable dan gunakaan serializer seperti ObjectOutputStream. Apa saja yang akan disimpan? 
Seluruh properties dari sebuah objek akan disimpan, tentunya properties tersebut harus Serializable juga.

Nah, bagaimana kalau ada property yang tidak ingin disimpan? 
ternyata cukup menggunakan keyword transient saja! Contohnya adalah sebagai berikut

*/
import java.io.Serializable;

public class Transient implements Serializable{
    private String nama;
    private transient String password;

    public Transient(String nama, String password) {
         //you know lah apa isinya
    } 
}

/*

Anggaplah saya punya kelas bernama belajarTransient yang akan digunakan untuk menyimpan nama dan password. 
Jika objek dari kelas tersebut diserialisasikan, maka informasi nama dan password akan disimpan.
Namun, jika saya tidak ingin informasi password disimpan,
maka keyword transient cukup ditambahkan pada deklarasi variabel password.
Contoh di atas akan berubah menjadi seperti berikut.

*/