/*

Keyword ini biasanya digunakan dalam suatu block program. 
keyword ini digunakan untuk mencoba menjalankan block program 
kemudian mengenai dimana munculnya kesalahan yang ingin diproses.
Keyword ini juga harus dipasangkan dengan keyword catch atau keyword finally


Contoh penggunaan :


*/

public class BelajarTry
{
    public static void main(String[] args) {
        try
        {
            int a = 1 / 0; // berpotensi untuk menimbulkan kesalahan yaitu
            // pembagian dengan bilangan 0
            System.out.println("perintah selanjutnya");
        }
        catch (Exception kesalahan)
        {
            System.err.println(kesalahan);
        }
    }
}

/* 

Perhatikan contoh diatas, ada beberapa hal penting yang perlu dilihat. 
Pertama, block program yag diyakini menimbulkan kesalahan maka ada di dalam block try and catch.
Kedua, kesalahan yang muncul akan dianggap sebagai object dan ditangkap catch kemudian di assign 
ke variable kesalahan dengan tipe Exception. 
Ketiga, perintah setelah munculnya kesalahan pada block try tidak akan dieksekusi.

*/